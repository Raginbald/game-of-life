let world;
let next;
let index;
let x;
let y;
let sum;
let scale = 2;
let scaleX;
let scaleY;

function setup()
{
  createCanvas(500, 500);
  frameRate(48);
  x = floor(width / scale);
  y = floor(height / scale);
  world = new Array(x).fill(0).map(
    x_ => Array(y).fill(0));
  next = new Array(x).fill(0).map(
    x_ => Array(y).fill(0));
  index = new Array(x).fill(0).map(
    x_ => Array(y).fill(0).map(
    y_ => Array(8).fill(0).map(
    h_ => Array(2).fill(0))));
  scaleX = new Array(x);
  scaleY = new Array(y);
  for (let i = 0; i < x; i++)
  {
    scaleX[i] = i * scale;
    for (let j = 0; j < y; j++)
    {
      scaleY[j] = j * scale;
      world[i][j] = floor(random(2));
      let xm1 = (i - 1 + x) % x;
      let xp1 = (i + 1 + x) % x;
      let ym1 = (j - 1 + y) % y;
      let yp1 =  (j + 1 + y) % y;
      index[i][j][0][0] = xm1;
      index[i][j][0][1] = ym1;
      index[i][j][1][0] = i;
      index[i][j][1][1] = ym1;
      index[i][j][2][0] = xp1;
      index[i][j][2][1] = ym1;
      index[i][j][3][0] = xm1;
      index[i][j][3][1] = j;
      index[i][j][4][0] = xp1;
      index[i][j][4][1] = j;
      index[i][j][5][0] = xm1;
      index[i][j][5][1] = yp1;
      index[i][j][6][0] = i;
      index[i][j][6][1] = yp1;
      index[i][j][7][0] = xp1;
      index[i][j][7][1] = yp1;
    }
  }
}

function draw()
{
  background(255);
  for (let i = 0; i < x; i++)
  {
    for (let j = 0; j < y; j++)
    {
      if (world[i][j])
      {
        fill(0);
        noStroke(0);
        rect(scaleX[i],
             scaleY[j],
             scale,
             scale);
      }
      sum += world[index[i][j][0][0]][index[i][j][0][1]];
      sum += world[i][index[i][j][1][1]];
      sum += world[index[i][j][2][0]][index[i][j][2][1]];
      sum += world[index[i][j][3][0]][j];
      sum += world[index[i][j][4][0]][j];
      sum += world[index[i][j][5][0]][index[i][j][5][1]];
      sum += world[i][index[i][j][6][1]];
      sum += world[index[i][j][7][0]][index[i][j][7][1]];
      next[i][j] = (world[i][j] && ( sum < 2 || sum > 3) ? 0 : world[i][j]);
      next[i][j] = (!world[i][j] && sum == 3) || next[i][j];
      sum = 0;
    }
  }
  let tmp = world;
  world = next;
  next = tmp;
}